function cursorStileOnHover(el, e) {
    var id = $(el).attr("id");
    var div = document.querySelector("#" + id);
    function finishDrag() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
    var cursor = "";
    var delta = 10; // the thickness of the hovered border areaF
    var rect = div.getBoundingClientRect();
    var x = e.clientX - rect.left, // the relative mouse postion to the element
            y = e.clientY - rect.top, // ...
            w = rect.right - rect.left, // width of the element
            h = rect.bottom - rect.top;     // height of the element

    var c = "";                          // which cursor to use
    if (y < delta) {
        c += "n";              // north
      // $(el).height($(el).height() + difY);
    } else if (y > h - delta) {
        c += "s";    // south
       // $(el).height($(el).height() + difY);
    }
    if (x < delta) {
        c += "w";              // west
       // $(el).width($(el).width() + difX);
    } else if (x > w - delta) {
        c += "e";     // east
       // $(el).width($(el).width() + difX);
    }
    if (c){                              // if we are hovering at the border area (c is not empty)
        div.style.cursor = c + "-resize"; // set the according cursor
        cursor = "resize";
    }
    else {                                 // otherwise
        div.style.cursor = "pointer";    // set to pointer
        cursor = "pointer";
    }
    div.addEventListener("mousedown", () => {
        if (cursor === "pointer") {
            div.style.position = "absolute";
            document.onmousemove = function (e) {
                div.style.top = (div.offsetTop + e.movementY) + "px";
                div.style.left = (div.offsetLeft + e.movementX) + "px";
            }
            document.onmouseup = finishDrag;
        }
        else if (cursor === "resize"){
            document.onmousemove = function (e) {
                if( $(el).css('cursor')== 'n-resize' ) {
                    $(el).height($(el).height() - e.movementY);
                    div.style.top = (div.offsetTop + e.movementY) + "px";
                }
                else if($(el).css('cursor')== 'w-resize'){
                    $(el).width($(el).width() - e.movementX);
                    div.style.left = (div.offsetLeft + e.movementX) + "px";
                }
                else if ($(el).css('cursor')== 'pointer' ) {
                    finishDrag();
                }
                else{
                    $(el).width($(el).width() + e.movementX);
                    $(el).height($(el).height() + e.movementY);
                }
            }     
            document.onmouseup = finishDrag;    
        }                
    });
}
